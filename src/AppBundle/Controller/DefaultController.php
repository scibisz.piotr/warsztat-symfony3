<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Template
     */
    public function indexAction()
    {
        $this->getDoctrine()->getManager();
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
        ));
    }

    /**
     * @Route("/dump_db", name="dump_db")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function dumpDBAction()
    {
        $path = $this->get('kernel')->getRootDir() . '/../web';
        $backupFile = $path . '/backup/renoart_5_' . date("Y-m-d") . '.sql';

        $mail = new \PHPMailer();
        $mail->setFrom('renoart@renoart.com', 'Kopia DB');
        $mail->addAddress('abusay8@gmail.com');
        $mail->addCC('scibisz.piotr@gmail.com');
        $mail->Subject = 'Baza danych z ' . date("Y-m-d");
        $mail->Body    = 'Baza danych z ' . date("Y-m-d") . ' jest w załączniku.';
        $mail->addAttachment($backupFile);

        if ($mail->send()) {
            $this->addFlash('notice', 'E-mail z kopią bazy danych został wysłany.');
        } else{
            $this->addFlash('notice', 'Error wysyłki');
        }

        $response = new Response();
        // Set headers
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', mime_content_type($backupFile));
        $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($backupFile) . '";');
        $response->headers->set('Content-length', filesize($backupFile));

        // Send headers before outputting anything
        $response->sendHeaders();

        return $response->setContent(file_get_contents($backupFile));
    }
}

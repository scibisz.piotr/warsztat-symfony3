<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Note;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use DateTime;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use AppBundle\Entity\Mechanic;
use AppBundle\Entity\Repairs;
use AppBundle\Entity\Cars;
use AppBundle\Entity\Parts;

/**
 * @Route("/car/repair")
 * @Security("has_role('ROLE_USER')")
 */
class RepairsController extends Controller
{
	/**
	 * @Route("/edit/{id}/{hide}", name="edit_repair")
	 * @Template("AppBundle:repairs:edit.html.twig")
	 */
	public function editAction($id, $hide = 'no', Request $request){
		$repair = $this->getDoctrine()->getRepository('AppBundle:Repairs')->find($id);

		if($hide != 'yes'){
		$form = $this->createFormBuilder($repair)
			->setMethod('POST')
			->setAction($this->generateUrl('edit_repair', array('id' => $id)))
			->add('mileage', IntegerType::class, array('label'  => 'Stan licznika', 'required' => false, 'attr' => array('class' => 'form-control', 'maxlength' => 11)))
			->add('description', TextareaType::class, array('label'  => 'Opis', 'required' => false, 'attr' => array('class' => 'form-control')))
			->add('created_at', DateType::class, array('label'  => 'Data', 'format' => 'dd-MM-yyy'))
			->add('Zapisz', SubmitType::class)
			->getForm();
		}
		else {
			$form = $this->createFormBuilder($repair)
				->setMethod('POST')
				->setAction($this->generateUrl('edit_repair', array('id' => $id, 'hide' => 'yes')))
				->add('description', TextareaType::class, array('label'  => 'Uwagi', 'required' => false, 'attr' => array('class' => 'form-control')))
				->add('Zapisz', SubmitType::class)
				->getForm();
		}

		if ($request->isMethod('POST')) {
			$form->handleRequest($request);

			if ($form->isSubmitted() && $form->isValid()) {
				if($hide == 'yes'){
					$repairOld = $this->getDoctrine()->getRepository('AppBundle:Repairs')->find($id);
					$repair->setName($repairOld->getName());
					$repair->setMileage($repairOld->getMileage());
					$repair->setCreatedAt($repairOld->getCreatedAt());
				}
				$em = $this->getDoctrine()->getManager();
				$repair->setUpdatedAt(new \DateTime());
				$em->persist($repair);
				$em->flush();

				return $this->redirect($this->generateUrl('show_repairs', array('id' => $repair->getId())));
			}
		}

		return array('form' => $form->createView(), 'id' => $id);
	}

	/**
	 * @Route("/add/{id}", name="add_repair")
	 * @Template("AppBundle:repairs:add.html.twig")
	 */
	public function addAction($id, Request $request){
		$repair = new Repairs();

		$form = $this->createFormBuilder($repair)
			->setMethod('POST')
			->setAction($this->generateUrl('add_repair', array('id' => $id)))
			->add('mileage', IntegerType::class, array('label'  => 'Stan licznika', 'required' => false, 'attr' => array('class' => 'form-control', 'maxlength' => 11)))
			->add('description', TextareaType::class, array('label'  => 'Uwagi', 'required' => false, 'attr' => array('class' => 'form-control')))
			->add('Zapisz', SubmitType::class)
			->getForm();

		if ($request->isMethod('POST')) {
			$form->handleRequest($request);

			if ($form->isSubmitted() && $form->isValid()) {
				$car = $this->getDoctrine()->getRepository('AppBundle:Cars')->find($id);
				$repair->setCreatedAt(new \DateTime());
				$repair->setCar($car);
				$repair->setEnabled(1);
				$em = $this->getDoctrine()->getManager();
				$em->persist($repair);
				$em->flush();

				return $this->redirect($this->generateUrl('show_repairs', array('id' => $repair->getId())));
			}
		}

		return array('form' => $form->createView());
	}

	/**
	 * @Route("/desabled/{id}", name="desabled_repairs")
	 * @Template()
	 */
	public function desabledAction($id){
		$repair = $this->getDoctrine()->getRepository('AppBundle:Repairs')->find($id);
		$repair->setUpdatedAt(new \DateTime());
		$repair->setEnabled(false);
		$em = $this->getDoctrine()->getManager();
		$em->persist($repair);
		$em->flush();

		return $this->redirect($this->generateUrl('car', array('id' => $repair->getCar()->getId())));
	}

	/**
	 * @Route("/show/{id}", name="show_repairs")
	 * @Template("AppBundle:repairs:show.html.twig")
	 */
	public function showAction($id){
		$repair = $this->getDoctrine()->getRepository('AppBundle:Repairs')->find($id);
		$mechanics = $this->getDoctrine()->getRepository('AppBundle:Mechanic')->findByEnabled(true);
		$secMechanic = null;
		if($repair->getName() && is_numeric($repair->getName())) {
			$secMechanic = $this->getDoctrine()->getRepository('AppBundle:Mechanic')->find($repair->getName());
		}
		$parts = $this->getDoctrine()->getRepository('AppBundle:Parts')->findBy(array('repair' => $repair));
		$notes = $this->getDoctrine()->getRepository('AppBundle:Note')->findBy(array('repairId' => $repair));
		return array('repair' => $repair, 'car' => $repair->getCar(), 'mechanics' => $mechanics, 'mechanicsCount' => count($mechanics), 'parts' => $parts, 'notes' => $notes, 'sec_mechanic' => $secMechanic);
	}

	/**
	 * @Route("/saveMechanicToRepair/{repairId}", name="save_mechanic_to_repair")
	 */
	public function saveMechanicToRepair($repairId, Request $request){
	//	$request = $this->get('request');
		if($request->isMethod('POST')){
			$delMechanic = htmlspecialchars($request->request->get('deleteMechanic'));
			if(!$delMechanic){
				$mechanicID = htmlspecialchars($request->request->get('mechanic'));
				$mechanic = $this->getDoctrine()->getRepository('AppBundle:Mechanic')->find($mechanicID);
			}
			else{
				$mechanic = null;
			}
			$repair = $this->getDoctrine()->getRepository('AppBundle:Repairs')->find($repairId);
			$repair->setMechanic($mechanic);
			$em = $this->getDoctrine()->getManager();
			$em->persist($repair);
			$em->flush();
		}
		return $this->redirect($this->generateUrl('show_repairs', array('id' => $repair->getId())));
	}

	/**
	 * @Route("/savePartToRepair/{repairId}", name="save_part_to_repair")
	 */
	public function savePartToRepair($repairId, Request $request){
	//	$request = $this->get('request');
		if($request->isMethod('POST')){
			$em = $this->getDoctrine()->getManager();

			$part = htmlspecialchars($request->request->get('part'));
			$partPrice = htmlspecialchars($request->request->get('part-price'));

			$repair = $this->getDoctrine()->getRepository('AppBundle:Repairs')->find($repairId);

			$partNow = new Parts();
			$partNow->setName($part);
			$partNow->setPrice((int)$partPrice);
			$partNow->setRepair($repair);
			$partNow->setCreatedAt(new \DateTime());
			$em->persist($partNow);
			$em->flush();
		}
		return $this->redirect($this->generateUrl('show_repairs', array('id' => $repair->getId())));
	}

	/**
	 * @Route("/editPart/{id}", name="edit_part")
	 * @Template("AppBundle:repairs:editPart.html.twig")
	 */
	public function editPartAction($id, Request $request){
		$part = $this->getDoctrine()->getRepository('AppBundle:Parts')->find($id);

		$form = $this->createFormBuilder($part)
				->setMethod('POST')
				->setAction($this->generateUrl('edit_part', array('id' => $id)))
				->add('name', TextType::class, array('label'  => 'Nazwa', 'attr' => array('class' => 'form-control', 'maxlength' => 255)))
				->add('price', IntegerType::class, array('label'  => 'Cena', 'required' => false, 'attr' => array('class' => 'form-control', 'maxlength' => 11)))
				->add('Zapisz', SubmitType::class)
				->getForm();

		if ($request->isMethod('POST')) {
			$form->handleRequest($request);

			if ($form->isSubmitted() && $form->isValid()) {
				$part->setUpdatedAt(new \DateTime());
				$em = $this->getDoctrine()->getManager();
				$em->persist($part);
				$em->flush();

				return $this->redirect($this->generateUrl('show_repairs', array('id' => $part->getRepair()->getId())));
			}
		}
		return array('form' => $form->createView(), 'id' => $id);
	}

	/**
	 * @Route("/editNote/{id}/{repairId}", name="edit_note")
	 * @Template("AppBundle:repairs:editNote.html.twig")
	 */
	public function editNoteAction($id = -1, $repairId, Request $request){
		if($id > 0) {
			$note = $this->getDoctrine()->getRepository('AppBundle:Note')->find($id);
		}else {
			$note = new Note();
		}
		$form = $this->createFormBuilder($note)
			->setMethod('POST')
			->setAction($this->generateUrl('edit_note', array('id' => $id, 'repairId' => $repairId)))
			->add('notatka', TextareaType::class, array('label'  => 'Treść', 'attr' => array('class' => 'form-control', 'maxlength' => 2000)))
			->add('Zapisz', SubmitType::class)
			->getForm();

		if ($request->isMethod('POST')) {
			$form->handleRequest($request);

			if ($form->isSubmitted() && $form->isValid()) {
				$note->setUpdatedAt(new \DateTime());
				$note->setRepairId(htmlspecialchars($repairId));
				$note->setOldId(-1);
				$em = $this->getDoctrine()->getManager();
				$em->persist($note);
				$em->flush();

				return $this->redirect($this->generateUrl('show_repairs', array('id' => $this->getDoctrine()->getRepository('AppBundle:Repairs')->find($note->getRepairId())->getId())));
			}
		}
		return array('form' => $form->createView(), 'id' => $id);
	}

	/**
	 * @Route("/deleteNote/{id}", name="delete_note")
	 */
	public function deletePart($id){
		$em = $this->getDoctrine()->getManager();

		$note = $this->getDoctrine()->getRepository('AppBundle:Note')->find($id);
		$repair_id = $note->getRepairId();
		$em->remove($note);
		$em->flush();

		return $this->redirect($this->generateUrl('show_repairs', array('id' => $this->getDoctrine()->getRepository('AppBundle:Repairs')->find($repair_id)->getId())));
	}

	/**
	 * @Route("/deletePart/{id}", name="delete_part")
	 */
	public function deleteNote($id){
		$em = $this->getDoctrine()->getManager();

		$part = $this->getDoctrine()->getRepository('AppBundle:Parts')->find($id);
		$repairId = $part->getRepair()->getId();
		$em->remove($part);
		$em->flush();

		return $this->redirect($this->generateUrl('show_repairs', array('id' => $repairId)));
	}

	/**
	 * @Route("/print/{id}", name="print")
	 * @Template("AppBundle:repairs:print.html.twig")
	 */
	public function printAction($id){
		$repair = $this->getDoctrine()->getRepository('AppBundle:Repairs')->find($id);
		$mechanics = $this->getDoctrine()->getRepository('AppBundle:Mechanic')->findByEnabled(true);
		$parts = $this->getDoctrine()->getRepository('AppBundle:Parts')->findBy(array('repair' => $repair->getId()));
		return array('repair' => $repair, 'car' => $repair->getCar(), 'mechanics' => $mechanics, 'mechanicsCount' => count($mechanics), 'parts' => $parts);
	}

	/**
	 * @Route("/add2Mechanic/{repair}", name="add_2_mechanic")
	 * @Method("POST")
	 */
	public function addSecendMechanicAction($repair, Request $request){
		$mechanic_id = $request->request->get('mechanicID');
		$repair = $this->getDoctrine()->getRepository('AppBundle:Repairs')->find($repair);
		$em = $this->getDoctrine()->getManager();
		$repair->setName($mechanic_id);
		$em->persist($repair);
		$em->flush();
		return $this->redirect($this->generateUrl('show_repairs', array('id' => $repair->getId())));
	}

	/**
	 * @Route("/del2Mechanic/{repair}", name="del_2_mechanic")
	 */
	public function delSecendMechanicAction($repair) {
		$repair = $this->getDoctrine()->getRepository('AppBundle:Repairs')->find($repair);
		$em = $this->getDoctrine()->getManager();
		$repair->setName(null);
		$em->persist($repair);
		$em->flush();
		return $this->redirect($this->generateUrl('show_repairs', array('id' => $repair->getId())));
	}
}

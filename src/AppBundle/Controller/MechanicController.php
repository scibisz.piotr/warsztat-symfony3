<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use DateTime;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use AppBundle\Entity\Mechanic;

/**
 * @Route("/mechanic")
 * @Security("has_role('ROLE_USER')")
 */
class MechanicController extends Controller
{
	/**
	 * @Route("/all", name="all_mechanic")
	 * @Template("AppBundle:mechanic:all.html.twig")
	 */
	public function allAction(){
		return array('mechanic' => $this->getDoctrine()->getRepository('AppBundle:Mechanic')->findByEnabled(true));
	}

	/**
	 * @Route("/create", name="create_mechanic")
	 * @Template("AppBundle:mechanic:create.html.twig")
	 */
	public function createAction(Request $request){
		$mechanic = new Mechanic();

		$form = $this->createFormBuilder($mechanic)
			->setMethod('POST')
			->setAction($this->generateUrl('create_mechanic'))
			->add('name', TextType::class, array('label'  => 'Nazwa', 'attr' => array('class' => 'form-control', 'maxlength' => 30)))
			->add('firstname', TextType::class, array('label'  => 'Imię', 'attr' => array('class' => 'form-control', 'maxlength' => 30)))
			->add('lastname', TextType::class, array('label'  => 'Nazwisko', 'attr' => array('class' => 'form-control', 'maxlength' => 30)))
			->add('phone', NumberType::class, array('label'  => 'Telefon', 'attr' => array('class' => 'form-control', 'maxlength' => 11)))
			->add('Zapisz', SubmitType::class)
			->getForm();

		if ($request->isMethod('POST')) {
			$form->handleRequest($request);
			if ($form->isSubmitted() && $form->isValid()) {
				$mechanic->setCreatedAt(new \DateTime());
				$mechanic->setEnabled(true);
				$em = $this->getDoctrine()->getManager();
				$em->persist($mechanic);
				$em->flush();

				return $this->redirect($this->generateUrl('all_mechanic'));
			}
		}

		return array('form' => $form->createView());
	}

	/**
	 * @Route("/edit/{id}", name="edit_mechanic")
	 * @Template("AppBundle:mechanic:edit.html.twig")
	 */
	public function editAction($id, Request $request){
		$mechanic = $this->getDoctrine()->getRepository('AppBundle:Mechanic')->find($id);

		$form = $this->createFormBuilder($mechanic)
			->setMethod('POST')
			->setAction($this->generateUrl('edit_mechanic', array('id' => $id)))
			->add('name', TextType::class, array('label'  => 'Nazwa', 'attr' => array('class' => 'form-control', 'maxlength' => 30)))
			->add('firstname', TextType::class, array('label'  => 'Imię', 'attr' => array('class' => 'form-control', 'maxlength' => 30)))
			->add('lastname', TextType::class, array('label'  => 'Nazwisko', 'attr' => array('class' => 'form-control', 'maxlength' => 30)))
			->add('phone', NumberType::class, array('label'  => 'Telefon', 'attr' => array('class' => 'form-control', 'maxlength' => 11)))
			->add('Zapisz', SubmitType::class)
			->getForm();

		if ($request->isMethod('POST')) {
			$form->handleRequest($request);

			if ($form->isSubmitted() && $form->isValid()) {
				$mechanic->setCreatedAt(new \DateTime());
				$em = $this->getDoctrine()->getManager();
				$em->persist($mechanic);
				$em->flush();

				return $this->redirect($this->generateUrl('all_mechanic'));
			}
		}

		return array('form' => $form->createView());
	}

	/**
	 * @Route("/deleted/{id}", name="desabled_mechanic")
	 * @Template("AppBundle:mechanic:all.html.twig")
	 */
	public function desebledAction($id){
		$mechanic = $this->getDoctrine()->getRepository('AppBundle:Mechanic')->find($id);
		$mechanic->setEnabled(false);
		$em = $this->getDoctrine()->getManager();
		$em->persist($mechanic);
		$em->flush();

		$mechanics = $this->getDoctrine()->getRepository('AppBundle:Mechanic')->findByEnabled(true);
		return array('mechanic' => $mechanics);
	}

	/**
	 * @Route("/stat", name="stat_mechanic")
	 * @Security("has_role('ROLE_ADMIN')")
	 * @Template("AppBundle:mechanic:stat.html.twig")
	 */
	public function statAction(Request $request){
		$mechanic_id = $request->query->get('mechanic');
		$okres = $request->query->get('okres') ? htmlspecialchars($request->query->get('okres')) : '';

		$time = null;
		if(!empty($okres)){
			switch ($okres) {
				case 'day':
					$time = date("Y-m-d", strtotime("-1 day"));
					break;
				case '1tydzien':
					$time = date("Y-m-d", strtotime("-1 week"));
					break;
				case '2tydzien':
					$time = date("Y-m-d", strtotime("-2 week"));
					break;
				case '3tydzien':
					$time = date("Y-m-d", strtotime("-3 week"));
					break;
				case '1mies':
					$time = date("Y-m-d", strtotime("-1 month"));
					break;
				case '2mies':
					$time = date("Y-m-d", strtotime("-2 month"));
					break;
				case '3mies':
					$time = date("Y-m-d", strtotime("-3 month"));
					break;
			}
		}

		$parts = [];
		if(!empty($mechanic_id)){
			$repairs = $this->getDoctrine()->getRepository('AppBundle:Repairs')->findBy(array('mechanic' => $mechanic_id, 'enabled' => true), ['id' => 'desc']);
			if(!empty($repairs)){
				$em = $this->getDoctrine()->getManager();
				if($time){
					$query = $em->createQuery(
						'SELECT p FROM AppBundle:Parts p WHERE ((p.name LIKE :usluga OR p.name LIKE :wymiana)
							AND p.repair IN (:ids))
							AND p.createdAt > :time
							ORDER BY p.id DESC')
						->setParameter('ids', $repairs)
						->setParameter('time', $time)
						->setParameter('usluga', '%usługa%')
						->setParameter('wymiana', '%wymiana%');
				}else {
					$query = $em->createQuery('SELECT p FROM AppBundle:Parts p WHERE ((p.name LIKE :usluga OR p.name LIKE :wymiana) AND p.repair IN (:ids)) ORDER BY p.id DESC')->setParameter('ids', $repairs)->setParameter('usluga', '%usługa%')->setParameter('wymiana', '%wymiana%');
				}
				$parts = $query->getResult();
			}
		}
		$mechanics = $this->getDoctrine()->getRepository('AppBundle:Mechanic')->findByEnabled(true);
		return ['mechanic' => $mechanics, 'mechanic_id' => $mechanic_id ? $mechanic_id : '', 'parts' => $parts, 'okres' => $okres];
	}

}

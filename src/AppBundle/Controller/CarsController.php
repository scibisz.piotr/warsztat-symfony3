<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use DateTime;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Entity\Cars;
use AppBundle\Entity\Repairs;
use AppBundle\Entity\Mechanic;

/**
 * @Route("/car")
 * @Security("has_role('ROLE_USER')")
 */
class CarsController extends Controller
{
	/**
	 * @Route("/", name="cars")
	 * @Template("AppBundle:cars:cars.html.twig")
	 */
	public function carsAction(){
		$cars = $this->getDoctrine()->getRepository('AppBundle:Repairs')->createQueryBuilder('c')
		    ->orderBy('c.createdAt', 'DESC')
			->andWhere('c.enabled = :en')
			->setParameter('en', true)
		    ->setMaxResults(100)
		    ->getQuery()->getResult();
		$last = [];
		$suma = [];
		foreach ($cars as $c) {
			if($c->getCar()->getEnabled()){
				$last[$c->getCreatedAt()->format('d-m-Y')][$c->getCar()->getId()] = $c->getCar();
				$s = $this->getDoctrine()->getRepository('AppBundle:Parts')->createQueryBuilder('p')
					->andWhere('p.repair = :id')
					->setParameter('id', $c->getId())
					->select('SUM(p.price)')
					->getQuery()
					->getOneOrNullResult();
				if(!empty($suma[$c->getCreatedAt()->format('d-m-Y')])) {
					$suma[$c->getCreatedAt()->format('d-m-Y')] = $suma[$c->getCreatedAt()->format('d-m-Y')] + $s[1];
				}else{
					$suma[$c->getCreatedAt()->format('d-m-Y')] = $s[1];
				}
			}
		}
		$countAllCars = $this->getDoctrine()->getRepository('AppBundle:Cars')->createQueryBuilder('c')
			->select('COUNT(c)')
			->getQuery()
			->getSingleScalarResult();

		$countAllRepairs = $this->getDoctrine()->getRepository('AppBundle:Repairs')->createQueryBuilder('r')
			->select('COUNT(r)')
			->getQuery()
			->getSingleScalarResult();

		return array('cars' => $last, 'search_value' => null, 'allCars' => $countAllCars, 'allRepairs' => $countAllRepairs, 'suma' => $suma);
	}

	/**
	 * @Route("/all", name="all_cars")
	 * @Template("AppBundle:cars:carsall.html.twig")
	 */
	public function allCarsAction(){
		$cars = $this->getDoctrine()->getRepository('AppBundle:Cars')->findAll();

		$countAllCars = $this->getDoctrine()->getRepository('AppBundle:Cars')->createQueryBuilder('c')
			->select('COUNT(c)')
			->getQuery()
			->getSingleScalarResult();

		$countAllRepairs = $this->getDoctrine()->getRepository('AppBundle:Repairs')->createQueryBuilder('r')
			->select('COUNT(r)')
			->getQuery()
			->getSingleScalarResult();

		return array('cars' => $cars, 'search_value' => null, 'allCars' => $countAllCars, 'allRepairs' => $countAllRepairs);
	}
	/**
	 * @Route("/{id}", name="car")
	 * @Template("AppBundle:cars:car.html.twig")
	 */
	public function carAction($id){
		$car = $this->getDoctrine()->getRepository('AppBundle:Cars')->find($id);
		$repairs = $this->getDoctrine()->getRepository('AppBundle:Repairs')->findBy(array('car' => $id, 'enabled' => true), ['id' => 'desc']);
		$price = $this->getDoctrine()->getRepository('AppBundle:Parts')->findBy(array('repair' => $repairs));
		$suma = null;
		foreach($price as $p){
			$suma = $suma + $p->getPrice();
		}
		return array('car' => $car, 'repairs' => $repairs, 'suma' => $suma);
	}

	/**
	 * @Route("/edit/{id}", name="car_edit")
	 * @Template("AppBundle:cars:edit.html.twig")
	 */
	public function editAction($id, Request $request){
		$car = $this->getDoctrine()->getRepository('AppBundle:Cars')->find($id);

		$form = $this->createFormBuilder($car)
			->setMethod('POST')
			->setAction($this->generateUrl('car_edit', array('id' => $id)))
			->add('name', TextType::class, array('label'  => 'Nazwa', 'attr' => array('class' => 'form-control')))
			->add('registration', TextType::class, array('label'  => 'Rejestrancja', 'attr' => array('class' => 'form-control', 'maxlength' => 30)))
			->add('vin', TextType::class, array('label'  => 'VIN', 'required' => false, 'attr' => array('class' => 'form-control', 'maxlength' => 20)))
			->add('phone', NumberType::class, array('label'  => 'Telefon', 'required' => false, 'attr' => array('class' => 'form-control', 'maxlength' => 11)))
			->add('description', TextareaType::class, array('label'  => 'Opis', 'required' => false, 'attr' => array('class' => 'form-control')))
			->add('Zapisz', SubmitType::class)
			->getForm();

		if ($request->isMethod('POST')) {
			$form->handleRequest($request);

			if ($form->isSubmitted() && $form->isValid()) {
				$car->setUpdatedAt(new \DateTime());
				$em = $this->getDoctrine()->getManager();
				$em->persist($car);
				$em->flush();

				return $this->redirect($this->generateUrl('car', array('id' => $id)));
			}
		}

		return array('form' => $form->createView(), 'car' => $car);
	}

	/**
	 * @Route("/add/car", name="add_car")
	 * @Template("AppBundle:cars:add.html.twig")
	 */
	public function addAction(Request $request){
		$car = new Cars();
		$form = $this->createFormBuilder($car)
			->setMethod('POST')
			->setAction($this->generateUrl('add_car'))
			->add('name', TextType::class, array('label'  => 'Nazwa', 'attr' => array('class' => 'form-control')))
			->add('registration', TextType::class, array('label'  => 'Rejestrancja', 'attr' => array('class' => 'form-control', 'maxlength' => 30)))
			->add('vin', TextType::class, array('label'  => 'VIN', 'required' => false, 'attr' => array('class' => 'form-control', 'maxlength' => 20)))
			->add('phone', NumberType::class, array('label'  => 'Telefon', 'required' => false, 'attr' => array('class' => 'form-control', 'maxlength' => 11)))
			->add('description', TextareaType::class, array('label'  => 'Opis', 'required' => false, 'attr' => array('class' => 'form-control')))
			->add('Zapisz', SubmitType::class)
			->getForm();

		if ($request->isMethod('POST')) {
			$form->handleRequest($request);

			if ($form->isSubmitted() && $form->isValid()) {
				$car->setCreatedAt(new \DateTime());
				$car->setUpdatedAt(new \DateTime());
				$car->setEnabled(1);
				$em = $this->getDoctrine()->getManager();
				$em->persist($car);
				$em->flush();

				return $this->redirect($this->generateUrl('car', array('id' => $car->getId())));
			}
		}

		return array('form' => $form->createView());
	}

	/**
	 * @Route("/car/search", name="search")
	 * @Template("AppBundle:cars:carsall.html.twig")
	 */
	public function searchAction(Request $request){
		$searchKey = htmlspecialchars($request->query->get('searchKey'));

		$em = $this->getDoctrine()->getManager();
		$query = $em->createQuery(
			'SELECT p FROM AppBundle:Cars p WHERE (p.vin LIKE :search OR p.registration LIKE :search OR p.name LIKE :search OR p.phone LIKE :search) AND p.enabled = 1 ORDER BY p.id ASC'
		)->setParameter('search', '%'.$searchKey.'%');

		$cars = $query->getResult();
		return array('cars' => $cars, 'search_value' => $searchKey);
	}

	/**
	 * @Route("/desabled/{id}", name="desabled")
	 * @Template("AppBundle:cars:cars.html.twig")
	 */
	public function desabledAction($id){
		$car = $this->getDoctrine()->getRepository('AppBundle:Cars')->find($id);
		$car->setUpdatedAt(new \DateTime());
		$car->setEnabled(false);
		$em = $this->getDoctrine()->getManager();
		$em->persist($car);
		$em->flush();

		$cars = $this->getDoctrine()->getRepository('AppBundle:Cars')->findByEnabled(true);
		return array('cars' => $cars);
	}

	/**
	 * @Route("/search_in_parts/{id}", name="search_in_parts")
	 * @Template("AppBundle:cars:car.html.twig")
	 */
	public function searchInPartsAction($id, Request $request){
		$searchKey = htmlspecialchars($request->query->get('searchKey'));

		$car = $this->getDoctrine()->getRepository('AppBundle:Cars')->find($id);
		$repairs = $this->getDoctrine()->getRepository('AppBundle:Repairs')->findBy(array('car' => $id, 'enabled' => true), ['id' => 'desc']);
		$price = $this->getDoctrine()->getRepository('AppBundle:Parts')->findBy(array('repair' => $repairs));
		$suma = null;
		foreach($price as $p){
			$suma = $suma + $p->getPrice();
		}


		$em = $this->getDoctrine()->getManager();
		$query = $em->createQuery(
			'SELECT p FROM AppBundle:Parts p WHERE p.name LIKE :search OR p.price LIKE :search ORDER BY p.id ASC'
		)->setParameter('search', '%'.$searchKey.'%');

		$search = $query->getResult();
		$res = [];
		foreach($search as $s){
			if($s->getRepair()->getCar()->getId() == $id && $s->getRepair()->getCar()->getEnabled() == true) {
				$res[$s->getRepair()->getId()] = $s;
			}
		}

		return array('car' => $car, 'repairs' => $repairs, 'suma' => $suma, 'search_parts_value' => $searchKey, 'resoults' => $res);
	}
}
<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use FOS\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Form;

/**
 * @Route("/user")
 * @Security("has_role('ROLE_ADMIN')")
 */
class userController extends Controller
{
    /**
     * @Route("/", name="all_users")
     * @Template("AppBundle:user:users.html.twig")
     */
    public function allUsersAction(){
        $userManager = $this->get('fos_user.user_manager');
        $all = $userManager->findUsers();
        return ['all' => $all];
    }
}

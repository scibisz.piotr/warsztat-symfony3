<?php
namespace AppBundle\Command;

use DateTime;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Entity\Cars;
use AppBundle\Entity\Repairs;
use AppBundle\Entity\Post;
use AppBundle\Entity\Parts;

class ConvertCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('my:convert')
            ->setDescription('Przenoszenie na nową db')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this -> getContainer() -> get('doctrine') -> getManager();
        $connection = $em->getConnection();

        $statement = $connection->prepare('SELECT * FROM posts WHERE pstref=0 ORDER BY postid LIMIT 200');

        $statement->execute();
        $samochods = $statement->fetchAll();

        foreach ($samochods as $sam)
        {   
            $samochod = new Cars();
            $samochod->setUpdatedAt(new \DateTime());
            $samochod->setCreatedAt(new \DateTime());
            $samochod->setPhone(null);
            $samochod->setRegistration($sam['subj']);
            $samochod->setVin($sam['naprawaid']);
            $samochod->setName($sam['text']);
            $samochod->setEnabled(true);

            $em->persist($samochod);
            $em->flush();

            $statement2 = $connection->prepare("SELECT * FROM posts where pstref= :id");
            $statement2->bindValue('id', $sam['postid']);
            $statement2->execute();
            $naprawy = $statement2->fetchAll();

            foreach ($naprawy as $naprawa)
            {
                $nap = new Repairs();
                $nap->setCreatedAt(new DateTime($naprawa['subj']));
                
                $sql = $connection->prepare("SELECT text FROM posts where pstref<>0 AND cena=0 AND pstref= :id AND text<>0");
                $sql->bindValue('id', $naprawa['postid']);
                $sql->execute();
                $przebieg = $sql->fetchAll();
                if(!empty($przebieg)){
                    $prz = (int)$przebieg[0]['text'];
                }else{
                    $prz = 0;
                }
                $nap->setMileage($prz);
                
                $sql = $connection->prepare("SELECT naprawaid FROM posts where pstref<>0 AND cena=0 AND pstref= :id AND text='' AND mechanik=1");
                $sql->bindValue('id', $naprawa['postid']);
                $sql->execute();
                $komentarz = $sql->fetchAll();
                if(!empty($komentarz)){
                    $kom = '';
                    foreach($komentarz as $i){
                        $kom .= ' ' . $i['naprawaid'];
                    }
                }else{
                    $kom = null;
                }
                $nap->setDescription($kom);
                $nap->setCar($samochod);
                $nap->setEnabled(true);

                $em->persist($nap);
                $em->flush();

                //notatki
                $sql = $connection->prepare("UPDATE note SET repair_id=:id WHERE old_id=:old_id");
                $sql->bindValue('id', $nap->getId());
                $sql->bindValue('old_id', $naprawa['postid']);
                $sql->execute();

                //czesci/uslugi
                $sql2 = $connection->prepare("SELECT * FROM posts where pstref<>0 AND pstref= :id AND mechanik=1 AND cena<>0 ORDER BY postid");
                $sql2->bindValue('id', $naprawa['postid']);
                $sql2->execute();
                $czesci = $sql2->fetchAll();
                
                foreach($czesci as $c)
                {
                    $czesc = new Parts();
                    $czesc -> setCreatedAt(new \DateTime());
                    $czesc->setPrice($c['cena']);
                    $czesc->setName($c['text']);
                    $czesc->setRepair($nap);
                    $em->persist($czesc);
                    $em->flush();
                }
            }

            //Usuwa samochód po skopiowaniu
            $sql0 = $connection->prepare("DELETE FROM `posts` WHERE subj=:subj AND naprawaid=:naprawaid");
            $sql0->bindValue('naprawaid', $sam['naprawaid']);
            $sql0->bindValue('subj', $sam['subj']);
            $sql0->execute();


        }
        $output->writeln('koniec');
    }
}
<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Note
 *
 * @ORM\Table(name="note")
 * @ORM\Entity
 */
class Note
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="notatka", type="text", length=65535, nullable=false)
     */
    private $notatka;

    /**
     * @var integer
     *
     * @ORM\Column(name="repair_id", type="integer", nullable=true)
     */
    private $repairId;

    /**
     * @var integer
     *
     * @ORM\Column(name="old_id", type="integer")
     */
    private $oldId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set notatka
     *
     * @param string $notatka
     *
     * @return Note
     */
    public function setNotatka($notatka)
    {
        $this->notatka = $notatka;

        return $this;
    }

    /**
     * Get notatka
     *
     * @return string
     */
    public function getNotatka()
    {
        return $this->notatka;
    }

    /**
     * Set repairId
     *
     * @param integer $repairId
     *
     * @return Note
     */
    public function setRepairId($repairId)
    {
        $this->repairId = $repairId;

        return $this;
    }

    /**
     * Get repairId
     *
     * @return integer
     */
    public function getRepairId()
    {
        return $this->repairId;
    }

    /**
     * Set oldId
     *
     * @param integer $oldId
     *
     * @return Note
     */
    public function setOldId($oldId)
    {
        $this->oldId = $oldId;

        return $this;
    }

    /**
     * Get oldId
     *
     * @return integer
     */
    public function getOldId()
    {
        return $this->oldId;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Note
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Note
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
